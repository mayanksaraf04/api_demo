class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.bigint :reporter_id, index: true #, foreign_key: { to_table: :users }
      t.bigint :assignee_id, index: true #, foreign_key: { to_table: :users }
      t.string :summary
      t.integer :status

      t.timestamps
    end
  end
end