FactoryGirl.define do
  factory :user do
    name { Faker::Name.name  }
    email { Faker::Internet.email }

    trait :invalid_email do
      email { nil }
    end

    trait :invalid_name do
      name { nil }
    end

  end
end
