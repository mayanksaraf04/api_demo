FactoryGirl.define do
  factory :issue do
    association :reporter, factory: :user
    association :assignee, factory: :user

    summary { Faker::Lorem.sentence }
    status { %i[start panding completed].sample.to_s }

    trait :invalid_reporter do
      reporter { nil }
    end

    trait :invalid_assignee do
      assignee { nil }
    end

    trait :invalid_summary do
      summary { nil }
    end

    trait :invalid_status do
      status { nil }
    end

  end
end
