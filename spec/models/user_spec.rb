require 'rails_helper'

RSpec.describe User, type: :model do
  context 'associations' do
    it { should have_many(:reported_issues) }
    it { should have_many(:assigned_issues) }
  end

  context 'validations' do
    context 'email should be valid' do
      let(:valid_object) { build(:user) }
      let(:invalid_object) { build(:user, :invalid_email) }
      it { expect(valid_object.email).to be_present }
      it { expect(invalid_object.email).not_to be_present }
    end

    context 'name should be valid' do
      let(:valid_object) { build(:user) }
      let(:invalid_object) { build(:user, :invalid_name) }
      it { expect(valid_object.name).to be_present }
      it { expect(invalid_object.name).not_to be_present }
    end

  end
end
