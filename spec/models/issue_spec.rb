require 'rails_helper'

RSpec.describe Issue, type: :model do
  context 'associations' do
    it { should belong_to(:reporter) }
    it { should belong_to(:assignee) }
  end

  context 'enum field' do
    it { expect define_enum_for(:status).with(%i[start panding completed]) }
  end

  context 'validations' do
    context 'status should be present valid' do
      let(:valid_object) { build(:issue) }
      let(:invalid_object) { build(:issue, :invalid_status) }
      it { expect(valid_object.status).to be_present }
      it { expect(invalid_object.status).not_to be_present }
    end

    context 'summary should be present valid' do
      let(:valid_object) { build(:issue) }
      let(:invalid_object) { build(:issue, :invalid_summary) }
      it { expect(valid_object.summary).to be_present }
      it { expect(invalid_object.summary).not_to be_present }
    end

    context 'reporter should be present valid' do
      let(:valid_object) { build(:issue) }
      let(:invalid_object) { build(:issue, :invalid_reporter) }
      it { expect(valid_object.reporter).to be_present }
      it { expect(invalid_object.reporter).not_to be_present }
    end

    context 'assignee should be present valid' do
      let(:valid_object) { build(:issue) }
      let(:invalid_object) { build(:issue, :invalid_assignee) }
      it { expect(valid_object.assignee).to be_present }
      it { expect(invalid_object.assignee).not_to be_present }
    end
  end



  context 'search method' do
    before(:each) do
      user1 = User.create! attributes_for(:user, name: 'user1')
      user2 = User.create! attributes_for(:user, name: 'user2')      
      @issue1 =  Issue.create(summary:'abcd', status: 'start'  , assignee_id: user1.id, reporter_id: user2.id )
      @issue2 =  Issue.create(summary:'abcd', status: 'panding', assignee_id: user1.id, reporter_id: user2.id ) 
      @issue3 =  Issue.create(summary:'defg', status: 'start'  , assignee_id: user2.id, reporter_id: user1.id ) 
      @issue4 =  Issue.create(summary:'defg', status: 'panding', assignee_id: user2.id, reporter_id: user1.id ) 
    end     
    
    context 'check search params' do

      it "search by assignee" do
        search_params = {assignee: 'user1'}
        expect(Issue.search(search_params).count).to eq(2)
        expect(Issue.search(search_params)).to eq([@issue2, @issue1])
      end

      it "search by reporter" do
        search_params = {reporter: 'user1'}
        expect(Issue.search(search_params).count).to eq(2)
        expect(Issue.search(search_params)).to eq([@issue4, @issue3])
      end

      it "search by summary" do
        search_params = {summary: 'abcd'}
        expect(Issue.search(search_params).count).to eq(2)
        expect(Issue.search(search_params)).to eq([@issue2, @issue1])
      end

      it "search by status" do
        search_params = {status: 'start'}
        expect(Issue.search(search_params).count).to eq(2)
        expect(Issue.search(search_params)).to eq([@issue3, @issue1])
      end
    
    end

    context 'check order params' do

      it "default order created_by desc" do
        search_params = {}
        order_params = {}
        expect(Issue.search(search_params, order_params)).to eq([@issue4, @issue3, @issue2, @issue1])
      end

      it "custom order created_by desc" do
        search_params = {}
        order_params = {sort_order: 'DESC'}
        expect(Issue.search(search_params, order_params)).to eq([@issue4, @issue3, @issue2, @issue1])
      end

      it "custom order created_by asc" do
        search_params =  {}
        order_params = {sort_order: 'ASC'}
        expect(Issue.search(search_params, order_params)).to eq([@issue1, @issue2, @issue3, @issue4])
      end

    end
  end  

  context 'check page params' do

    before(:each) do
      user1 = User.create! attributes_for(:user, name: 'user1')
      user2 = User.create! attributes_for(:user, name: 'user2')
      @issues = []  
      (1..100).to_a.each do |index|
        @issues << Issue.create(summary:"summary#{index}", status: 'start'  , assignee_id: user1.id, reporter_id: user2.id )
      end
      @issues = @issues      
    end     

    it "default page 1 and default page_size 25" do
      search_params = {}
      order_params = {sort_order: 'ASC'}
      page_params = {}
      expect(Issue.search(search_params, order_params, page_params).count).to eq(25)
      expect(Issue.search(search_params, order_params, page_params).map(&:id)).to eq(@issues[0..24].map(&:id))
    end

    it "default page 1 and custom page_size 50" do
      search_params = {}
      order_params = {sort_order: 'ASC'}
      page_params = { page_size: 50 }
      expect(Issue.search(search_params, order_params, page_params).count).to eq(50)
      expect(Issue.search(search_params, order_params, page_params).map(&:id)).to eq(@issues[0..49].map(&:id))
    end

    it "custom page 2 and custom page_size 50" do
      search_params = {}
      order_params = {sort_order: 'ASC'}
      page_params = { page: 2, page_size: 50 }
      expect(Issue.search(search_params, order_params, page_params).count).to eq(50)
      expect(Issue.search(search_params, order_params, page_params).map(&:id)).to eq(@issues[50..99].map(&:id))
    end

    it "custom page 3 and default page_size 25" do
      search_params = {}
      order_params = {sort_order: 'ASC'}
      page_params = { page: 3 }
      expect(Issue.search(search_params, order_params, page_params).count).to eq(25)
      expect(Issue.search(search_params, order_params, page_params).map(&:id)).to eq(@issues[50..74].map(&:id))
    end

  end

end