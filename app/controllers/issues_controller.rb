class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :update, :destroy]
  before_action :search_params, only: [:search]
  before_action :order_params, only: [:search]
  before_action :page_params, only: [:search]

  # GET /issues/search
  def search
    @issues = Issue.search(search_params, order_params, page_params)
    render json: @issues
  end

  # GET /issues
  def index
    @issues = Issue.all

    render json: @issues
  end

  # GET /issues/1
  def show
    render json: @issue
  end

  # POST /issues
  def create
    @issue = Issue.new(issue_params)

    if @issue.save
      render json: @issue, status: :created, location: @issue
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /issues/1
  def update
    if @issue.update(issue_params)
      render json: @issue
    else
      render json: @issue.errors, status: :unprocessable_entity
    end
  end

  # DELETE /issues/1
  def destroy
    @issue.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    def search_params
      params.permit(:summary, :assignee, :reporter, :status)
    end

    def order_params
      params.permit(:sort_order)
    end

    def page_params
      params.permit(:page, :page_size)
    end

    # Only allow a trusted parameter "white list" through.
    def issue_params
      params.require(:issue).permit(:reporter_id, :assignee_id, :summary, :status)
    end
end
