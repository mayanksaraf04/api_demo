class Issue < ApplicationRecord

  # Asosciation 
  belongs_to :reporter, class_name: 'User'
  belongs_to :assignee, class_name: 'User'  

  # Enum Field
  enum status: %i[start panding completed]

  # Validation 
  validates :reporter_id, :assignee_id, :status, :summary, presence: true

  # Scope 
  scope :search_by_summary, ->(summary) { where('summary LIKE ?', "%#{summary}%") }
  scope :search_by_assignee, ->(assignee_name) { includes(:assignee).references(:users).where('users.name LIKE ?', "%#{assignee_name}%" ) }
  scope :search_by_reporter, ->(reporter_name) { includes(:reporter).references(:users).where('users.name LIKE ?', "%#{reporter_name}%" ) }
  scope :search_by_status, ->(status) { where('status = ?', statuses[status.to_sym] ) }

  def self.search(search_params={}, order_params={}, page_params={})
    issues = self
    search_params.each do |search_key, search_val|
      issues = issues.send("search_by_#{search_key}".to_sym, search_val) unless search_val.blank? 
    end
    issues = issues.order("issues.created_at #{order_params[:sort_order] || 'DESC'}")
    issues.page(page_params[:page] || 1).per(page_params[:page_size] || 25)
  end  

end
