class User < ApplicationRecord
  # Asosciation 
  has_many :reported_issues , class_name: 'Issue', foreign_key: 'reporter_id'
  has_many :assigned_issues , class_name: 'Issue', foreign_key: 'assignee_id'

  # Validation 
  validates :email, :name, presence: true
  validates :email, uniqueness: true
  validates :email, email: true

end
